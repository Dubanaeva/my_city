from django.views.generic import ListView, TemplateView, DetailView
from reports.models import Report, ReportImage


class DashBoardView(TemplateView):
    template_name = "dashboard_home.html"


class LoginView(TemplateView):
    template_name = "login.html"


class AllReportsView(ListView):
    model = Report
    template_name = "webpanels/report_list.html"

class SingleReportView(DetailView):
    model = Report
    template_name = "webpanels/report_detail.html"