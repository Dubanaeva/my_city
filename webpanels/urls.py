from django.urls import path, re_path
from .views import DashBoardView, AllReportsView, SingleReportView, LoginView

app_name = 'webpanels'

urlpatterns = [
    path('', DashBoardView.as_view(), name='dashboard'),
    path('reports/<int:pk>/', SingleReportView.as_view(), name='report'),
    path('reports/', AllReportsView.as_view(), name='reports'),
]