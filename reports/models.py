from django.contrib.gis.db.models import PointField
from django.db import models


class Report(models.Model):
	title = models.CharField(max_length = 150)
	text = models.TextField()
	# location_latlng = PointField() # for postgres
	location = models.JSONField(default=str)
	created_at = models.DateTimeField(auto_now_add=True)
	edited_at = models.DateTimeField(auto_now=True)
	

	def __str__(self):
		return f"{self.pk} - {self.title}"


class ReportImage(models.Model):
	img = models.ImageField(upload_to='report_images')
	report = models.ForeignKey(Report, on_delete=models.CASCADE)
	